A statistical analysis and coding-for-fun project

## Motivation

This project started as a simple assignment during my internship in which they gave me a task to analyse a simple sentence from a JFK's speech.
Curiosity got the best of me and I started to explore a little bit more of the data there.

## What is it about?
The project can be divided roughly in two parts.

First half is a simple tokenization of the the speeches of the Presidents of the United States of America alongside a prediction model.

Second half is an ongoing statistical analysis of various quantities of the words of the president such as Gunning-fog,Smog  and Flesch Kincaid indexes.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Acknowledgments
Special thank you to the following but not limited:

[Panos Ipeirotis](https://github.com/ipeirotis) for his precious help with his readability endpoint.


[Giannis Avramidis](https://gitlab.com/giannisav91) for his  thoughts on regex.
And many more..
