package com.ipeirotis.readability.endpoints;

import com.ipeirotis.readability.engine.Readability;
import com.ipeirotis.readability.enums.MetricType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;


public class ReadabilityEndpoint {


    public static Map<MetricType, BigDecimal> get(String text) {
        Map<MetricType, BigDecimal> result = new HashMap<MetricType, BigDecimal>();
        Readability r = new Readability(text);

        for (MetricType metricType : MetricType.values()) {
            BigDecimal value = new BigDecimal(Double.toString(r.getMetric(metricType)));
            value = value.setScale(3, RoundingMode.HALF_UP);
            result.put(metricType, value);
        }

        return result;
    }

    public static StringBuilder getWords(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        StringBuilder president = new StringBuilder();
        String st;
        while ((st = br.readLine()) != null) {
            president.append(st);
        }
        return (president);
    }

}