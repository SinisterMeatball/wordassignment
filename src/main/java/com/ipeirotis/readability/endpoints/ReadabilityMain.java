package com.ipeirotis.readability.endpoints;


import com.ipeirotis.readability.enums.MetricType;
import importTextHelper.Constants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.ipeirotis.readability.endpoints.ReadabilityEndpoint.getWords;
import static java.util.stream.Collectors.toMap;
import static writeToCsv.generateCsvFile.findPoliticalParty;


public class ReadabilityMain {
    public static void main(String[] args) throws IOException {
        writeToCsv();
    }

    private static Map<MetricType, BigDecimal> presidentImplementation(String presidentName) throws IOException {

        File[] files = new File(Constants.pathForSpeeches + presidentName).listFiles();
        String str = null;
        for (File file : files) {


            StringBuilder words = getWords(file);
            str = str + words;


        }
        return ReadabilityEndpoint.get(str);
    }

    private static void writeToCsv() throws IOException {
        File[] files = new File(Constants.pathForSpeeches).listFiles();
        FileWriter writer = new FileWriter(new File(Constants.pathForAmericanPresidents, "GeneralInfoForPresidents.csv"), true);
        writer.append("President Name");
        writer.append(',');
        writer.append("Political Party");
        writer.append(',');
        writer.append("Sentences");
        writer.append(',');
        writer.append("Complex Words");
        writer.append(',');
        writer.append("Words");
        writer.append(',');
        writer.append("Syllables");
        writer.append(',');
        writer.append("Characters");
        writer.append(',');
        writer.append("Smog Index");
        writer.append(',');
        writer.append("Coleman-Liau");
        writer.append(',');
        writer.append("Gunning fog");
        writer.append(',');
        writer.append("ARI");
        writer.append(',');
        writer.append("Flesch Kincaid");
        writer.append(',');
        writer.append("Flesch Reading");
        writer.append(',');
        writer.append("Smog");
        writer.append('\n');
        for (var file : files) {
            Map<MetricType, BigDecimal> presidentInfo = presidentImplementation(file.getName());
            writer.append(file.getName());
            writer.append(',');
            writer.append(findPoliticalParty(file.getName()));
            writer.append(',');
            presidentInfo = orderTheMap(presidentInfo);
            for (Map.Entry<MetricType, BigDecimal> entry : presidentInfo.entrySet()) {
                System.out.println(entry.getKey());
                writer.append(entry.getValue().toString());
                writer.append(',');


            }
            writer.append('\n');

        }
        writer.flush();
        writer.close();

    }

    private static Map<MetricType, BigDecimal> orderTheMap(Map<MetricType, BigDecimal> wordMap) {
        return wordMap.entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByKey()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }

}
