package importTextHelper;

public class Constants {
    public static final String pathForSpeeches = "C:\\Users\\User\\IdeaProjects\\wordassignment\\resources\\speeches\\";
    public static final String pathForAmericanPresidents = "C:\\Users\\User\\IdeaProjects\\wordassignment\\exports\\americanPresidents\\";
    public static final String pathForTwitter = "C:\\Users\\User\\IdeaProjects\\wordassignment\\exports\\TwitterResults\\";
    public static final String pathForNLP = "C:\\Users\\User\\IdeaProjects\\wordassignment\\exports\\openNlpDataFile\\";
    public static final String pathForInfo = "C:\\Users\\User\\IdeaProjects\\wordassignment\\resources\\generalInfo";

}
