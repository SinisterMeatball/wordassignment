package opennlpstuff;

import importTextHelper.Constants;
import writeToCsv.generateCsvFile;

public class AutomatedProcessToOpenNLPData {
    public static void main(String[] args) {


        automatedProcessToOpenNLPData();
    }

    private static void automatedProcessToOpenNLPData() {


        generateCsvFile.writeToTxtEveryWordForNLP("OpenNLPdata.txt", Constants.pathForSpeeches, Constants.pathForNLP);
        generateCsvFile.writeForNLPWithPresidentName("dataWithPresidentName.txt", Constants.pathForSpeeches, Constants.pathForNLP);
    }
}
