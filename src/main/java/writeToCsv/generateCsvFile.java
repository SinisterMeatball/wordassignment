package writeToCsv;

import importTextHelper.Constants;
import importTextHelper.GetWords;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static importTextHelper.GetWords.parseFilesForFivePercent;

public interface generateCsvFile {
    static void writeToCsvTop10percent(String filename, String presidentName, String pathForFiles, String directory) {
        try {
            FileWriter writer = new FileWriter(new File(directory, filename));

            HashMap<String, Integer> presidentWords = new HashMap<>();
            AddInPresidentWords(presidentWords, pathForFiles);

            writer.append(presidentName);
            writer.append(',');
            writer.append("Occurences");
            writer.append('\n');
            for (Map.Entry<String, Integer> entry : presidentWords.entrySet()) {

                writer.append(entry.getKey());
                writer.append(',');
                writer.append(entry.getValue().toString());
                writer.append('\n');
            }


            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static void writeToCsvEveryWord(String filename, String presidentName, String pathForFiles, String directory) {
        try {
            FileWriter writer = new FileWriter(new File(directory, filename));

            HashMap<String, Integer> presidentWords = new HashMap<>();
            AddInEveryWord(presidentWords, pathForFiles);

            writer.append(presidentName);
            writer.append(',');
            writer.append("Occurences");
            writer.append('\n');
            for (Map.Entry<String, Integer> entry : presidentWords.entrySet()) {

                writer.append(entry.getKey());
                writer.append(',');
                writer.append(entry.getValue().toString());
                writer.append('\n');
            }


            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void writeToTxtEveryWordForNLP(String filename, String pathForFiles, String directory) {
        try {
            FileWriter writer = new FileWriter(new File(directory, filename));

            HashMap<String, Integer> presidentWords = new HashMap<>();
            File[] files = new File(Constants.pathForSpeeches).listFiles();
            for (File file : files) {

                AddInEveryWord(presidentWords, Constants.pathForSpeeches + file.getName());

                writer.append(findPoliticalParty(file.getName()));
                for (Map.Entry<String, Integer> entry : presidentWords.entrySet()) {


                    writer.append("  ");
                    for (int i = 0; i < entry.getValue(); i++) {
                        writer.append(entry.getKey());
                        writer.append("  ");

                    }


                }


                writer.append("\n");
                presidentWords.clear();
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String findPoliticalParty(String presidentName) throws IOException {
        File[] files = new File(Constants.pathForInfo).listFiles();
        BufferedReader br = new BufferedReader(new FileReader(files[0]));


        String row;
        String politicalParty = null;
        int counter = 1;
        readInfo:
        {
            while ((row = br.readLine()) != null) {

                if (counter != 1) {
                    String[] data = row.split(",");

                    if (data[0].equals(presidentName)) {
                        System.out.println(data[0]);
                        politicalParty = data[1];
                        System.out.println(data[1]);
                        break readInfo;
                    }
                } else {
                    counter += 1;
                }


            }

        }
        return politicalParty;
    }

    static void writeForNLPWithPresidentName(String filename, String pathForFiles, String directory) {
        try {
            FileWriter writer = new FileWriter(new File(directory, filename));

            HashMap<String, Integer> presidentWords = new HashMap<>();
            File[] files = new File(Constants.pathForSpeeches).listFiles();
            for (File file : files) {

                AddInEveryWord(presidentWords, Constants.pathForSpeeches + file.getName());

                writer.append(file.getName());
                for (Map.Entry<String, Integer> entry : presidentWords.entrySet()) {


                    writer.append("  ");
                    for (int i = 0; i < entry.getValue(); i++) {
                        writer.append(entry.getKey());
                        writer.append("  ");

                    }


                }


                writer.append("\n");
                presidentWords.clear();
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    static HashMap<String, Integer> AddInPresidentWords(HashMap<String, Integer> presidentWords, String
            pathForFiles) throws IOException {

        File[] files = new File(pathForFiles).listFiles();

        parseFilesForFivePercent(files, presidentWords);
        return presidentWords;
    }

    static HashMap<String, Integer> AddInEveryWord(HashMap<String, Integer> presidentWords, String pathForFiles) throws
            IOException {
        File[] files = new File(pathForFiles).listFiles();

        GetWords.parseFilesForAddingEveryWord(files, presidentWords);
        return presidentWords;
    }
}
