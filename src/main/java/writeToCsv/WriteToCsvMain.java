package writeToCsv;

import importTextHelper.Constants;

import java.io.File;

public class WriteToCsvMain {
    public static void main(String[] args) {

        File[] files = new File(Constants.pathForSpeeches).listFiles();

        for (File file : files) {
            automatedProcessToWriteToCsv(file.getName());

        }
    }

    private static void automatedProcessToWriteToCsv(String presidentName) {

        File dir = new File(Constants.pathForAmericanPresidents + presidentName);
        dir.mkdir();
        generateCsvFile.writeToCsvEveryWord(presidentName + "EveryWordSaid.csv", presidentName, Constants.pathForSpeeches + presidentName, Constants.pathForAmericanPresidents + presidentName);
        generateCsvFile.writeToCsvTop10percent(presidentName + "MostUsedWords.csv", presidentName, Constants.pathForSpeeches + presidentName, Constants.pathForAmericanPresidents + presidentName);

    }


}
